# JS Training Exercises

1.	JS Drum

o	Play the sound that's associated with that key 
o	Add animation where it pops the button up to be a little bit bigger when a sound is played
o	Apply yellow border and a background

2.	Make an analog clock by using only CSS and JS.
The idea that we're going for here is that we're going to apply a rotate to each of the hands depending on what time it currently is.
3.	Array Operations 

Using the provided arrays do the following:
o	Filter the list of inventors for those who were born in the 1500's
o	Give us an array of the inventors' first and last names
o	Sort the inventors by birthdate, oldest to youngest
o	How many years did all the inventors live? 
o	Sort the inventors by years lived
o	Sort the people alphabetically by last name
o	Sum up the instances of each of these (refers to a specific array)
o	Check if at least one person is 19 or older?
o	Check if everyone is 19 or older?
o	Find the comment with the ID of 823423
o	delete the comment with the ID of 823423
Methods of Array.prototype to be used: filter, map, sort, reduce, some, every, find, findIndex

4.	Flex Panel Gallery TODO: ATTACH SCREENSHOTS

We have flex panels and for them in the HTML, we've got a div with a class of panels, and each of these panels is going to be called a panel, singular. And then inside of each of those panels is going to be, "Hey," "Let's dance," "Give," "Take," "Receive." And these are going to be words that, when clicked, they're going to grow in size, as well as they're going to fall from the top and come up from the bottom. There is some CSS to start you with. If we open this up in the browser you see that the CSS is there, but it doesn't do anything, and it is not in the right place. You should use flexbox to fix it:

o	Get the panels side by side 
o	Make the panels to take as much room as they need (as big as the words are)
o	Distribute the in between extra space evenly among them
o	Center text vertically
o	Have each of the text items take a third of the vertical space
o	Hide all top and bottom words
o	On click, toggle enlarging / shrinking the clicked picture to take five times / as equal the amount of extra space than the rest (this will be the active picture)
o	When a picture is ‘active’ bring in the words from the top and bottom

5.	Key Sequence Detection

Listen for a six-character key sequence of your choice and when there is a match output a success message in the console.

6.	JavaScript References vs Copying

o	Make a copy of the provided array
o	When you edit the copy array is the original array changed as well? Why?
o	Provide at least two ways of fixing ‘the problem’
o	Make a copy of the provided object

7.	LocalStorage and Event Delegation
o	Dynamically add items (e.g. dishes) to a list using the provided interface
o	Clear the input field after each submit
o	Display the list in place of ‘Loading Tapas…’ text
o	Add checkbox before each item 
o	Persist the state on page refresh (both list items and checkboxes state)
	Make sure that you can keep the state of the checkbox for the newly added items right after they are added to the list

8.	Adding Up Times with Reduce
o	Pull the data times out of the dom
o	Convert them to numbers
o	Make them into minutes and seconds
o	Add them up to figure out how many hours, minutes and seconds they are in total
o	Console log the result

9.	Sticky Nav
o	Fix the navigation bar at the top of the window on scroll
o	When the nav is fixed, show the logo on the left as shown below (I’ve added it in the html and some styles for it in the css) 

10.	Event Capture, Propagation, Bubbling and Once

You are provided with three divs nested inside each other with classes “one”, “two” and “three”
o	Listen for a click on all each of the divs and console log the class of the clicked div
o	Do it by bulk selecting all elements that have ‘div’ tag
o	Loop through them to attach an event listener to each
o	What will happen if you click on the inner most (coral color)? Why?
o	What is the difference between Capture and Bubbling?
o	Can you run the logging function on Capture?
o	How can you stop Bubbling?
o	Have the event unbind itself after first execution
